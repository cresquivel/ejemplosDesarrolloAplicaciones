package model.tableModels;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.dao.PersonaDAO;
import model.entities.Persona;

public class PersonaTableModel extends AbstractTableModel {

	private String[] columnas = new String[] { "Id", "Nombre", "ApellidoPat", "ApellidoMat" };
	PersonaDAO dao = new PersonaDAO();
	List<Persona> personas;
	List<Persona> personasAux;

	public PersonaTableModel() {
		cargarTodos();
	}

	public void cargarTodos() {
		this.personas = dao.buscarTodos();
		personasAux = new ArrayList<Persona>(personas);
	}

	public void buscarPorCualquierCampo(String valor) {
		personasAux = new ArrayList<Persona>();
		String valorMin = valor.toLowerCase();
		for (Persona p : personas) {
			if (p.getNombre().toLowerCase().contains(valorMin)
					|| p.getApellidoPaterno().toLowerCase().contains(valorMin)
					|| p.getApellidoMaterno().toLowerCase().contains(valorMin)) {
				personasAux.add(p);
			}
		}
	}

	public void buscarPorNombreDesdeBaseDeDatos(String nombre) {
		this.personas = dao.buscarPorNombre(nombre);
	}

	@Override
	public String getColumnName(int columna) {
		return columnas[columna];
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 4;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return personasAux.size();
	}

	@Override
	public Object getValueAt(int fila, int columna) {
		// TODO Auto-generated method stub
		if (columna == 0) {
			return personasAux.get(fila).getId();
		} else if (columna == 1) {
			return personasAux.get(fila).getNombre();
		} else if (columna == 2) {
			return personasAux.get(fila).getApellidoPaterno();
		} else {
			return personasAux.get(fila).getApellidoMaterno();
		}
	}
}
