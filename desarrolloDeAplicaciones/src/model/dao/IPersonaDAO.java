package model.dao;

import java.util.List;

import model.entities.Persona;

public interface IPersonaDAO {
	
	public Persona agregar(Persona p);
	
	public void modificar(Persona p);
	
	public void eliminar(Persona p);
	
	public List<Persona> buscarTodos();
	
	public List<Persona> buscarPorNombre(String nombre);
	
	public List<Persona> buscarPorCualquierCampo(String nombre);

}
