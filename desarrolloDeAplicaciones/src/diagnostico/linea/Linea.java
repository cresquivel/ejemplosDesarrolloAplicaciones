package diagnostico.linea;

import diagnostico.punto.Punto;

public class Linea {

	private Punto punto1;
	private Punto punto2;
	private String unidad;

	public Linea(Punto punto1, Punto punto2) {
		if (punto1.getX() == punto2.getX() && punto1.getY() == punto2.getY()) {
			punto2.setX(punto2.getY() + 1);
		}
		this.punto1 = punto1;
		this.punto2 = punto2;
	}

	public Punto getPunto1() {
		return punto1;
	}

	public void setPunto1(Punto punto1) {
		this.punto1 = punto1;
	}

	public Punto getPunto2() {
		return punto2;
	}

	public void setPunto2(Punto punto2) {
		this.punto2 = punto2;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public double getLongitud() {
		int diferenciaX = punto2.getX() - punto1.getX();
		int diferenciaY = punto2.getY() - punto1.getY();

		int cuadradoDiferenciaX = diferenciaX * diferenciaX;
		int cuadradoDiferenciaY = diferenciaY * diferenciaY;

		int sumaCuadrados = cuadradoDiferenciaX + cuadradoDiferenciaY;

		double distancia = Math.sqrt(sumaCuadrados);

		return distancia;
	}

	public double getPendiente() {
		double pendiente = (punto2.getY() - punto1.getY()) 
				/ (punto2.getX() - punto1.getX());
		return pendiente;
	}

}
