package sockets;

import java.io.*;
import java.net.*;
import java.util.Scanner;

class TCPServer {
	public static void main(String argv[]) throws Exception {
		String clientSentence; // variable donde se almacena la petici�n
		String capitalizedSentence; // almacena la petici�n en may�sculas
		ServerSocket welcomeSocket = new ServerSocket(6789); // creamos objeto server socket

		for(;;){
			
			Socket connectionSocket = welcomeSocket.accept();
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
			clientSentence = inFromClient.readLine();
			System.out.println("Received: " + clientSentence);
			capitalizedSentence = clientSentence.toUpperCase() + "\n";
			Scanner scan = new Scanner(System.in);
			String respuesta = scan.nextLine()+ "\n";
			outToClient.writeBytes(respuesta);
		}
	}
}