package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class BuscadorPersona extends JFrame {

	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JTextField nombreTB;
	private JTextField apTB;
	private JTextField amTB;
	private JButton guardarBtn;
	private JTextField buscarTb;
	private JTextField buscarRAMTb;
	private JLabel lblBuscarRam;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BuscadorPersona frame = new BuscadorPersona();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BuscadorPersona() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 569, 447);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		scrollPane = new JScrollPane();
		scrollPane.setBounds(92, 215, 326, 183);
		contentPane.add(scrollPane);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(92, 29, 107, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellidoPaterno = new JLabel("Apellido Paterno");
		lblApellidoPaterno.setBounds(92, 64, 118, 14);
		contentPane.add(lblApellidoPaterno);
		
		JLabel lblApellidoMaterno = new JLabel("Apellido Materno");
		lblApellidoMaterno.setBounds(92, 104, 127, 14);
		contentPane.add(lblApellidoMaterno);
		
		nombreTB = new JTextField();
		nombreTB.setBounds(209, 29, 185, 20);
		contentPane.add(nombreTB);
		nombreTB.setColumns(10);
		
		apTB = new JTextField();
		apTB.setColumns(10);
		apTB.setBounds(209, 61, 185, 20);
		contentPane.add(apTB);
		
		amTB = new JTextField();
		amTB.setColumns(10);
		amTB.setBounds(209, 101, 185, 20);
		contentPane.add(amTB);
		
		guardarBtn = new JButton("Nuevo");
		guardarBtn.setBounds(425, 28, 107, 23);
		contentPane.add(guardarBtn);
		
		JButton btnNewButton = new JButton("modificar");
		btnNewButton.setBounds(425, 60, 107, 23);
		contentPane.add(btnNewButton);
		
		JButton btnEliminar = new JButton("eliminar");
		btnEliminar.setBounds(425, 95, 107, 23);
		contentPane.add(btnEliminar);
		
		buscarTb = new JTextField();
		buscarTb.setBounds(148, 159, 264, 20);
		contentPane.add(buscarTb);
		buscarTb.setColumns(10);
		
		JLabel lblBuscar = new JLabel("Buscar BD:");
		lblBuscar.setBounds(28, 162, 191, 14);
		contentPane.add(lblBuscar);
		
		buscarRAMTb = new JTextField();
		buscarRAMTb.setColumns(10);
		buscarRAMTb.setBounds(148, 190, 264, 20);
		contentPane.add(buscarRAMTb);
		
		lblBuscarRam = new JLabel("Buscar RAM:");
		lblBuscarRam.setBounds(28, 190, 191, 14);
		contentPane.add(lblBuscarRam);
		
	}


	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}
	
	public JTextField getNombreTB() {
		return nombreTB;
	}

	public JTextField getApTB() {
		return apTB;
	}

	public JTextField getAmTB() {
		return amTB;
	}

	public JButton getGuardarBtn() {
		return guardarBtn;
	}

	public JTextField getBuscarTb() {
		return buscarTb;
	}

	public JTextField getBuscarRAMTb() {
		return buscarRAMTb;
	}
	
	
	
}
