package controller;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTable;

import model.dao.PersonaDAO;
import model.entities.Persona;
import model.tableModels.PersonaTableModel;
import view.BuscadorPersona;

public class PersonaController {
	BuscadorPersona vista;
	PersonaTableModel personaTableModel;
	PersonaDAO dao = new PersonaDAO();
	JTable table;

	public PersonaController(BuscadorPersona vista, PersonaTableModel personaTableModel) {
		this.vista = vista;
		this.personaTableModel = personaTableModel;
		System.out.println(personaTableModel.getRowCount());
		table = new JTable(this.personaTableModel);
		vista.getScrollPane().setViewportView(table);
		vista.getGuardarBtn().addActionListener(e -> guardar());

		vista.getBuscarTb().addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				String nombre = PersonaController.this.vista.
						getBuscarTb().getText();
				PersonaController.this.buscarPorNombre(nombre);

			}
		});
		
		vista.getBuscarRAMTb().addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				String valor = PersonaController.this.vista.
						getBuscarRAMTb().getText();
				System.out.println(valor);
				PersonaController.this.buscarPorCualquierCampo(valor);
			}
		});
	}

	public void buscarPorCualquierCampo(String valor) {
		personaTableModel.buscarPorCualquierCampo(valor);
		table.setModel(personaTableModel);
		vista.getScrollPane().setViewportView(table);
	}
	public void buscarPorNombre(String nombre) {
		personaTableModel.buscarPorNombreDesdeBaseDeDatos(nombre);
		table.setModel(personaTableModel);
		vista.getScrollPane().setViewportView(table);
	}

	public void guardar() {
		Persona p = new Persona();
		p.setNombre(vista.getNombreTB().getText());
		p.setApellidoPaterno(vista.getApTB().getText());
		p.setApellidoMaterno(vista.getAmTB().getText());
		dao.agregar(p);
		personaTableModel.cargarTodos();
		table.setModel(personaTableModel);
		vista.getScrollPane().setViewportView(table);
	}

}
