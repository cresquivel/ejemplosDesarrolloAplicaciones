package si33.primerosEjemplosGUI;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

//Listener desde la clase contenedora
public class MiVentanita extends JFrame implements ActionListener{
	
	private JButton miBoton;
	
	public MiVentanita(){
		this.setSize(400, 400);
		this.setTitle("Bazinga");
		
		miBoton = new JButton("Mi botoncito");
		this.setLayout(null);
		
		miBoton.setBounds(200, 200, 150, 40);
		this.getContentPane().add(miBoton);
		
		MiPrimerListener listener1 = new MiPrimerListener();
		MiSegundoListener listener2 = new MiSegundoListener();

		miBoton.addActionListener(this);
		
		miBoton.addActionListener(listener2);
		miBoton.addActionListener(listener1);
		
		miBoton.setBackground(Color.BLUE);
		miBoton.setForeground(Color.white);
		
		
		this.setVisible(true);
	}

	public static void main(String[] args) {
		MiVentanita mv = new MiVentanita();
		

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("Yo soy otro oyente");
		miBoton.setText(":)");
		miBoton.setBackground(Color.red);
		
	}

}
