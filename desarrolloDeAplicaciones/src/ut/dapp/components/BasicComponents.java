package ut.dapp.components;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ut.dapp.models.Alumno;

import javax.swing.JTextField;
import java.awt.TextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class BasicComponents extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BasicComponents frame = new BasicComponents();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BasicComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JComboBox comboString = new JComboBox();
		comboString.setBounds(137, 57, 231, 20);
		comboString.addItem("Cesar");
		comboString.addItem("Pedro");
		comboString.addItem("Damian");
		contentPane.add(comboString);
		
		JComboBox comboObject = new JComboBox();
		comboObject.setBounds(137, 88, 231, 20);
		Alumno a1 = new Alumno("Cesar", "Costa", 56);
		Alumno a2 = new Alumno("Pedro", "Almodovar", 70);
		Alumno a3 = new Alumno("Damian", "Alcazar", 48);
		contentPane.add(comboObject);
		comboObject.addItem(a1);
		comboObject.addItem(a2);
		comboObject.addItem(a3);
		
		JComboBox comboModel = new JComboBox();
		comboModel.setModel(new DefaultComboBoxModel(new String[] {"Cesar", "Pedro", "Damian"}));
		comboModel.setBounds(137, 119, 231, 20);
		contentPane.add(comboModel);
		
		JComboBox comboModelObject = new JComboBox();
		comboModelObject.setBounds(137, 150, 231, 20);
		comboModelObject.setModel(new DefaultComboBoxModel(new Alumno[] {a1, a2, a3}));
		contentPane.add(comboModelObject);

		
		

		
		
	}
}
