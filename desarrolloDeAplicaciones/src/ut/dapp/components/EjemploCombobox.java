package ut.dapp.components;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ut.dapp.models.Persona;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;

public class EjemploCombobox extends JFrame {

	private JPanel contentPane;
	private JComboBox comboBox;
	private JComboBox comboBox2;
	private JComboBox comboBox3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EjemploCombobox frame = new EjemploCombobox();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EjemploCombobox() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		comboBox = new JComboBox();
		comboBox.setBounds(123, 11, 263, 20);
		
		comboBox.addItem("Mario");
		comboBox.addItem("Luisa");
		comboBox.addItem("Diana");
		
		

		contentPane.add(comboBox);
		
		comboBox2 = new JComboBox();
		comboBox2.setBounds(123, 59, 263, 20);
		
		Persona p1 = new Persona("Mario", "Fuentes", "Puc");
		Persona p2 = new Persona("Luisa", "Guzm�n", "Luna");
		Persona p3 = new Persona("Diana", "Garc�a", "Silva");
		
		comboBox2.addItem(p1);
		comboBox2.addItem(p2);
		comboBox2.addItem(p3);
		
		contentPane.add(comboBox2);
		
		comboBox3 = new JComboBox();
		comboBox3.setModel(new DefaultComboBoxModel(new String[] {"Mario", "Luisa", "Diana"}));
		comboBox3.setBounds(123, 106, 263, 20);
		contentPane.add(comboBox3);
		
		JComboBox comboBox4 = new JComboBox();
		comboBox4.setBounds(123, 211, 263, 20);
		Persona[] personas = {p1,p2,p3};
		DefaultComboBoxModel model = new DefaultComboBoxModel(personas);
		comboBox4.setModel(model);
		contentPane.add(comboBox4);
		
		JLabel lblPersona = new JLabel("Persona:");
		lblPersona.setBounds(123, 185, 96, 14);
		contentPane.add(lblPersona);
	}
}
