package ut.dapp.components;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.ListModel;
import javax.swing.border.EmptyBorder;

import ut.dapp.models.Alumno;

import javax.swing.JList;

public class EjemploJList extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EjemploJList frame = new EjemploJList();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EjemploJList() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JList<Alumno> list = new JList<Alumno>();
		DefaultListModel<Alumno> model = new DefaultListModel<Alumno>();
		model.addElement(new Alumno("Cesar", "Costa", 56));
		model.addElement(new Alumno("Pedro", "Almodovar", 70));
		model.addElement(new Alumno("Damian", "Alcazar", 48));

		list.setModel(model);
		
		for(int i=0; i<model.size();i++) {
			Alumno a = model.getElementAt(i);
			System.out.println("El nombre del alumno es:"+ a.getNombre());
		}

		list.setBounds(105, 58, 217, 144);
		contentPane.add(list);
	}
}
