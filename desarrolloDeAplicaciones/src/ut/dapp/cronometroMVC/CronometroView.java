package ut.dapp.cronometroMVC;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class CronometroView {

	private JFrame frmCronometroMvc;
	private JButton iniciarBtn;
	private JButton pararBtn;
	private JButton reiniciarBtn;
	private JLabel tiempoLabel;

	public CronometroView() {
		initialize();
	}

	private void initialize() {
		frmCronometroMvc = new JFrame();
		frmCronometroMvc.setTitle("Cronometro MVC");
		frmCronometroMvc.setBounds(100, 100, 301, 355);
		frmCronometroMvc.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCronometroMvc.getContentPane().setLayout(null);
		tiempoLabel = new JLabel("00:00:00");
		tiempoLabel.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 60));
		tiempoLabel.setBounds(33, 22, 242, 70);
		frmCronometroMvc.getContentPane().add(tiempoLabel);

		iniciarBtn = new JButton("Iniciar");
		iniciarBtn.setBounds(59, 124, 176, 23);
		frmCronometroMvc.getContentPane().add(iniciarBtn);

		pararBtn = new JButton("Parar");
		pararBtn.setBounds(59, 158, 176, 23);
		frmCronometroMvc.getContentPane().add(pararBtn);

		reiniciarBtn = new JButton("Reiniciar");
		reiniciarBtn.setBounds(59, 192, 176, 23);
		frmCronometroMvc.getContentPane().add(reiniciarBtn);
		
		frmCronometroMvc.setVisible(true);
	}

	public JFrame getFrmCronometroMvc() {
		return frmCronometroMvc;
	}

	public JButton getIniciarBtn() {
		return iniciarBtn;
	}

	public JButton getPararBtn() {
		return pararBtn;
	}

	public JButton getReiniciarBtn() {
		return reiniciarBtn;
	}

	public JLabel getTiempoLabel() {
		return tiempoLabel;
	}

}
