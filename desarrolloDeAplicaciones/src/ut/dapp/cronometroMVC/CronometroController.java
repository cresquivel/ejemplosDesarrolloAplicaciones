package ut.dapp.cronometroMVC;

public class CronometroController extends Thread {

	private CronometroModel model;
	private CronometroView view;
	private boolean iniciado;

	public CronometroController(CronometroModel model, CronometroView view) {
		this.model = model;
		this.view = view;
	}

	public void inicializarControlador() {
		this.view.getIniciarBtn().addActionListener(e -> iniciado = true);
		this.view.getPararBtn().addActionListener(e -> {
			iniciado = false;
		});
		this.view.getReiniciarBtn().addActionListener(e -> reiniciarLabel());
		this.start();
	}

	public void run() {

		while (true) {
			if (iniciado) {
				incrementarSegundo();
			}
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void reiniciarLabel() {
		iniciado = false;
		model.asignarHora(0, 0, 0);
		view.getTiempoLabel().setText("00:00:00");
	}

	private void incrementarSegundo() {

		model.incrementarSegundos();

		StringBuilder construirHora = new StringBuilder();
		if (model.getHoras() < 10)
			construirHora.append("0");
		construirHora.append(model.getHoras());
		construirHora.append(":");
		if (model.getMinutos() < 10)
			construirHora.append("0");
		construirHora.append(model.getMinutos());
		construirHora.append(":");
		if (model.getSegundos() < 10)
			construirHora.append("0");
		construirHora.append(model.getSegundos());

		view.getTiempoLabel().setText(construirHora.toString());
	}
}
