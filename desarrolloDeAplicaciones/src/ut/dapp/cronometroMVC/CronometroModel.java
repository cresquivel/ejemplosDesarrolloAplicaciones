package ut.dapp.cronometroMVC;

public class CronometroModel {
	private int horas = 0;
	private int minutos = 0;
	private int segundos = 0;

	public CronometroModel(int horas, int minutos, int segundos) {
		this.setHoras(horas);
		this.setMinutos(minutos);
		this.setSegundos(segundos);
	}
	
	public void asignarHora(int horas, int minutos, int segundos) {
		this.setHoras(horas);
		this.setMinutos(minutos);
		this.setSegundos(segundos);
	}

	public int getHoras() {
		return horas;
	}

	public void setHoras(int horas) {
		if (horas >= 0 && horas <= 23) {
			this.horas = horas;
		}
	}

	public int getMinutos() {
		return minutos;
	}

	public void setMinutos(int minutos) {
		if (minutos >= 0 && minutos <= 59) {
			this.minutos = minutos;
		}
	}

	public int getSegundos() {
		return segundos;
	}

	public void setSegundos(int segundos) {
		if (segundos >= 0 && segundos <= 59) {
			this.segundos = segundos;
		}
	}

	public int incrementarHoras() {
		if (horas >= 23) {
			horas = 0;
		} else {
			horas++;
		}
		return horas;
	}

	public int incrementarMinutos() {
		if (minutos >= 59) {
			minutos = 0;
			this.incrementarHoras();
		} else {
			minutos++;
		}
		return minutos;
	}

	public int incrementarSegundos() {
		if (segundos >= 59) {
			segundos = 0;
			this.incrementarMinutos();
		} else {
			segundos++;
		}
		return segundos;
	}
}
