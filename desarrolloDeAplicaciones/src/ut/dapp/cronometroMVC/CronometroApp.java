package ut.dapp.cronometroMVC;

import javax.swing.SwingUtilities;

public class CronometroApp {

	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					CronometroView vista = new CronometroView();
					CronometroModel modelo = new CronometroModel(0,0,0);
					CronometroController controlador = new CronometroController(modelo, vista);
					controlador.inicializarControlador();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


}
