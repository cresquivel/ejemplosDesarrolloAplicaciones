package si32.primerosEjemplosGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

// Definiendo un Listener en la clase contenedora
public class MiPrimeraVentanita extends JFrame implements ActionListener {
	private JButton miBoton;
	private JButton miBoton2;

	MiPrimeraVentanita() {
		this.setTitle("Holaaaaaa Enfermera :P");
		this.setSize(300, 300);
		this.setResizable(true);
		this.setLayout(null);

		miBoton = new JButton("click me");
		miBoton.setBounds(150, 150, 120, 40);
		this.getContentPane().add(miBoton);

		miBoton2 = new JButton("click me too");
		miBoton2.setBounds(150, 200, 120, 40);
		this.getContentPane().add(miBoton2);

		// Intanciando el objeto que implementa ActionListener
		MiPrimerListener listener1 = new MiPrimerListener();
		MiSegundoListener listener2 = new MiSegundoListener();
		miBoton.addActionListener(this);
		miBoton2.addActionListener(this);

		this.setVisible(true);
	}

	public static void main(String args[]) {
		MiPrimeraVentanita mpv = new MiPrimeraVentanita();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == miBoton) {
			System.out.println("Yo soy el primer bot�n");
		} else if (e.getSource() == miBoton2) {
			System.out.println("Yo soy el segundo bot�n");
		}

	}
}
