package si32.primerosEjemplosGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class Sumadora extends JFrame implements ActionListener {
	private JTextField sumando1;
	private JTextField sumando2;
	private JTextField resultado;
	private JButton calcular;

	public Sumadora() {
		this.setTitle("Sumadora");
		this.setSize(500, 400);
		this.setLayout(null);

		sumando1 = new JTextField();
		sumando2 = new JTextField();
		resultado = new JTextField();
		calcular = new JButton("Calcular");

		sumando1.setBounds(50, 50, 200, 50);
		sumando2.setBounds(50, 150, 200, 50);
		resultado.setBounds(50, 250, 200, 50);
		calcular.setBounds(300, 250, 100, 50);

		this.getContentPane().add(sumando1);
		this.getContentPane().add(sumando2);
		this.getContentPane().add(resultado);
		this.getContentPane().add(calcular);

		resultado.setEnabled(false);
		calcular.addActionListener(this);

		this.setVisible(true);
	}

	public static void main(String args[]) {
		Sumadora s = new Sumadora();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		try {
			// TODO Auto-generated method stub
			int valor1 = Integer.parseInt(sumando1.getText());
			int valor2 = Integer.parseInt(sumando2.getText());
			Integer res = valor1 + valor2;
			resultado.setText(res.toString());
		} catch (Exception e) {
			System.out.println("Error en el formato num�rico");
			resultado.setText("0");
		}
	}
}