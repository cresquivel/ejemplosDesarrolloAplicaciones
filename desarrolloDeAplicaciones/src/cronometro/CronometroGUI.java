package cronometro;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CronometroGUI extends JFrame implements Runnable {

	private JPanel contentPane;

	private Cronometro cronometro;
	private JLabel label = new JLabel("00:00:00");
	private boolean iniciado = false;

	public static void main(String[] args) {
		(new Thread(new CronometroGUI())).start();
	}

	/**
	 * Create the frame.
	 */
	public CronometroGUI() {
		cronometro = new Cronometro(0, 0, 0);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton startButton = new JButton("Start");
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				iniciado = true;
			}
		});
		startButton.setBounds(65, 202, 89, 23);
		contentPane.add(startButton);

		JButton stopButton = new JButton("Stop");
		stopButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iniciado = false;
			}
		});
		stopButton.setBounds(177, 202, 89, 23);
		contentPane.add(stopButton);

		JButton resetButton = new JButton("Reset");
		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iniciado = false;
				reiniciarLabel();
			}
		});
		resetButton.setBounds(294, 202, 89, 23);
		contentPane.add(resetButton);

		label.setFont(new Font("Rockwell", Font.PLAIN, 60));
		label.setBounds(105, 33, 271, 122);
		contentPane.add(label);
		this.setVisible(true);
	}

	private void incrementarObtenerHoraCompleta() {

		cronometro.incrementarSegundos();

		StringBuilder construirHora = new StringBuilder();
		if (cronometro.getHoras() < 10) {
			construirHora.append("0");
		}
		construirHora.append(cronometro.getHoras());
		construirHora.append(":");
		if (cronometro.getMinutos() < 10)
			construirHora.append("0");
		construirHora.append(cronometro.getMinutos());
		construirHora.append(":");
		if (cronometro.getSegundos() < 10)
			construirHora.append("0");
		construirHora.append(cronometro.getSegundos());

		label.setText(construirHora.toString());
	}

	private void reiniciarLabel() {
		cronometro.asignarHora(0, 0, 0);
		label.setText("00:00:00");
	}

	@Override
	public void run() {

		while (true) {
			if (iniciado) {
				incrementarObtenerHoraCompleta();
			}
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
