package primerosEjemplos;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
public class TableExample extends JFrame
{
    public TableExample()
    {
        //headers for the table
        String[] columns = new String[] {
            "Id", "Nombre", "ApellidoPat", "ApellidoMat"
        };
         
        //actual data for the table in a 2d array
        Object[][] data = new Object[][] {
            {1, "Juan", "Perez", "Jimenez" },
            {2, "Carlos", "Gomez", "Garc�a" },
            {3, "Laura", "Barrera", "Saldivar" },
        };
        //create table with data
        JTable table = new JTable(data, columns);
         
        //add the table to the frame
        this.add(new JScrollPane(table));
         
        this.setTitle("Ejemplo Tabla");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);       
        this.pack();
        this.setVisible(true);
    }
     
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new TableExample();
            }
        });
    }   
}