package model.tableModels;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.dao.PersonaDAO;
import model.entities.Persona;

public class PersonaTableModel extends AbstractTableModel {

	PersonaDAO dao = new PersonaDAO();
	List<Persona> personas;

	public PersonaTableModel() {
		this.personas = dao.buscarTodos();
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 4;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return personas.size();
	}

	@Override
	public Object getValueAt(int fila, int columna) {
		// TODO Auto-generated method stub
		if (columna == 0) {
			return personas.get(fila).getId();
		} else if (columna == 1) {
			return personas.get(fila).getNombre();
		} else if (columna == 2) {
			return personas.get(fila).getApellidoPaterno();
		} else {
			return personas.get(fila).getApellidoMaterno();
		}
	}
}
