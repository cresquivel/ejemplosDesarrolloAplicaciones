package implementacion;

import controller.PersonaController;
import model.tableModels.PersonaTableModel;
import view.BuscadorPersona;

public class PersonaMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BuscadorPersona vista = new BuscadorPersona();
		PersonaTableModel modelo = new PersonaTableModel();
		PersonaController controlador = new PersonaController(vista, modelo);
		vista.setVisible(true);
	}

}
