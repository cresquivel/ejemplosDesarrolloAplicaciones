package controller;

import java.awt.ScrollPane;

import javax.swing.JScrollPane;

import model.tableModels.PersonaTableModel;
import view.BuscadorPersona;

public class PersonaController {
	BuscadorPersona vista;
	PersonaTableModel personaTableModel;
	public PersonaController(BuscadorPersona vista, PersonaTableModel personaTableModel) {
		this.vista = vista;
		this.personaTableModel = personaTableModel;
		System.out.println(personaTableModel.getRowCount());
		vista.getTable().setModel(personaTableModel);
		JScrollPane sp = new JScrollPane(vista.getTable());
		vista.getContentPane().add(sp);
	}
	
	

}
